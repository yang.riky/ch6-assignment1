const { Pool } = require('pg');
const pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'ch6_sql',
    password: '123456',
    port: 5432,
});
module.exports = {
    query: (text, params, callback) => {
        return pool.query(text, params, callback)
    },
}