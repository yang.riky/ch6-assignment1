var express = require('express');
var router = express.Router();
const db = require('../db');

router.post('/', function (req, res, next) {
    let query = `INSERT INTO articles(title, body, user_id) VALUES ($1, $2, $3)`;
    let values = [req.body.title, req.body.body, req.headers.user_id];

    db.query(query, values, (err, result) => {
        if (err) {
            res.json({
                error: err
            });
            return next(err);
        }
        res.status(200).json({
            message: "Sucessfully created article"
        });
    })
});

/* GET articles listing. */
router.get('/', function (req, res, next) {
    let query = `
    SELECT articles.id, articles.title, articles.body, articles.created_at, articles.updated_at, users.username as author, users.email as author_email 
    FROM articles 
    JOIN users ON articles.user_id = users.id 
    WHERE articles.deleted_at is null`;

    db.query(query, (err, result) => {
        if (err) {
            res.json({
                error: err
            });
            return next(err);
        }

        const data = result.rows;

        data.map((d) => {
            let queryComment = `
            SELECT comment, comments.created_at, username
            FROM comments
            JOIN users ON comments.user_id = users.id
            WHERE article_id = $1 AND comments.deleted_at is null`;
            let valuesComment = [d.id];
            db.query(queryComment, valuesComment, async (errComment, resultComment) => {
                if (errComment) {
                    res.json({
                        error: errComment
                    });
                    return next(errComment);
                }

                d.comments = resultComment.rows;
            });
        });

        res.status(200).json({
            data,
            message: "Get All Articles"
        });
    })
});

router.get('/:id', function (req, res, next) {
    let query = `
    SELECT articles.id, articles.title, articles.body, articles.created_at, articles.updated_at, users.username as author, users.email as author_email 
    FROM articles 
    JOIN users ON articles.user_id = users.id 
    WHERE articles.deleted_at is null AND articles.id = $1`;
    let values = [req.params.id];

    db.query(query, values, (err, result) => {
        if (err) {
            res.json({
                error: err
            });
            return next(err);
        }

        let data = result.rows[0];

        let queryComment = `
            SELECT comment, comments.created_at, username
            FROM comments
            JOIN users ON comments.user_id = users.id
            WHERE article_id = $1 AND comments.deleted_at is null`;
        let valuesComment = [data.id];
        db.query(queryComment, valuesComment, (errComment, resultComment) => {
            if (errComment) {
                res.json({
                    error: errComment
                });
                return next(errComment);
            }
            data.comments = resultComment.rows;

            res.status(200).json({
                data,
                message: "Get Article"
            });
        })
    })
});

router.put('/:id', function (req, res, next) {
    let query = `
    UPDATE articles SET 
    title = $1, 
    body = $2, 
    approved = $3, 
    updated_at = CURRENT_TIMESTAMP 
    WHERE id = $4 AND user_id = $5`;
    let values = [req.body.title, req.body.body, req.body.approved, req.params.id, req.headers.user_id];

    db.query(query, values, (err, result) => {
        console.log(values);
        if (err) {
            res.json({
                error: err
            });
            return next(err);
        }
        if (result.rowCount > 0) {
            res.status(200).json({
                message: "Sucessfully updated article"
            });
        } else {
            res.status(400).json({
                message: "Fail to update article"
            });
        }
    })
});

router.delete('/:id', function (req, res, next) {
    let query = `
    UPDATE articles SET 
    deleted_at = CURRENT_TIMESTAMP 
    WHERE id = $1 AND user_id = $2`;
    let values = [req.params.id, req.headers.user_id];

    db.query(query, values, (err, result) => {
        if (err) {
            res.json({
                error: err
            });
            return next(err);
        }

        if (result.rowCount > 0) {
            res.status(200).json({
                message: "Sucessfully deleted article"
            });
        } else {
            res.status(400).json({
                message: "Fail to delete article"
            });
        }
    })
});

module.exports = router;
