var express = require('express');
var router = express.Router();
const db = require('../db');

router.post('/', function (req, res, next) {
    let query = `INSERT INTO comments(comment, article_id, user_id) VALUES ($1, $2, $3)`;
    let values = [req.body.comment, req.body.article_id, req.headers.user_id];

    db.query(query, values, (err, result) => {
        if (err) {
            res.json({
                error: err
            });
            return next(err);
        }
        res.status(200).json({
            message: "Sucessfully created comment"
        });
    })
});

router.put('/:id', function (req, res, next) {
    let query = `
    UPDATE comments SET 
    comment = $1,
    updated_at = CURRENT_TIMESTAMP 
    WHERE id = $2 AND user_id = $3`;
    let values = [req.body.comment, req.params.id, req.headers.user_id];

    db.query(query, values, (err, result) => {
        console.log(values);
        if (err) {
            res.json({
                error: err
            });
            return next(err);
        }
        if (result.rowCount > 0) {
            res.status(200).json({
                message: "Sucessfully updated comment"
            });
        } else {
            res.status(400).json({
                message: "Fail to update comment"
            });
        }
    })
});

router.delete('/:id', function (req, res, next) {
    let query = `
    UPDATE comments SET 
    deleted_at = CURRENT_TIMESTAMP 
    WHERE id = $1 AND user_id = $2`;
    let values = [req.params.id, req.headers.user_id];

    db.query(query, values, (err, result) => {
        if (err) {
            res.json({
                error: err
            });
            return next(err);
        }

        if (result.rowCount > 0) {
            res.status(200).json({
                message: "Sucessfully deleted comment"
            });
        } else {
            res.status(400).json({
                message: "Fail to delete comment"
            });
        }
    })
});

module.exports = router;
