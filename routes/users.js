var express = require('express');
var router = express.Router();
const db = require('../db');

router.post('/', function (req, res, next) {
  let query = "INSERT INTO users(username, email, password) VALUES ($1, $2, $3)";
  let values = [req.body.username, req.body.email, req.body.password];

  db.query(query, values, (err, result) => {
    if (err) {
      res.json({
        error: err
      });
      return next(err);
    }
    res.status(200).json({
      message: "Sucessfully created user"
    });
  })
});

/* GET users listing. */
router.get('/', function (req, res, next) {
  let query = "SELECT username, email FROM users";

  db.query(query, (err, result) => {
    if (err) {
      res.json({
        error: err
      });
      return next(err);
    }
    res.status(200).json({
      data: result.rows,
      message: "Get All Users"
    });
  })
});

router.get('/:id', function (req, res, next) {
  let query = "SELECT username, email FROM users WHERE id = $1";
  let values = [req.params.id];

  db.query(query, values, (err, result) => {
    if (err) {
      res.json({
        error: err
      });
      return next(err);
    }
    res.status(200).json({
      data: result.rows,
      message: "Get User"
    });
  })
});

router.put('/:id', function (req, res, next) {
  let query = `UPDATE users SET username=$1, email=$2, password=$3, updated_at=CURRENT_TIMESTAMP WHERE id = $4`;
  let values = [req.body.username, req.body.email, req.body.password, req.params.id];

  db.query(query, values, (err, result) => {
    if (err) {
      res.json({
        error: err
      });
      return next(err);
    }
    res.status(200).json({
      message: "Sucessfully updated user"
    });
  })
});

router.delete('/:id', function (req, res, next) {
  let query = `DELETE FROM users WHERE id = $1`;
  let values = [req.params.id];

  db.query(query, values, (err, result) => {
    if (err) {
      res.json({
        error: err
      });
      return next(err);
    }
    res.status(200).json({
      message: "Sucessfully deleted user"
    });
  })
});

module.exports = router;
